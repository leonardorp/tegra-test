import BaseRepository from './base';
import { AirportService } from '../service';
import { AirportEntity } from '../entity';

class AirportRepository extends BaseRepository {
  getServiceProvider() {
    return AirportService;
  }

  modelToEntity = model => new AirportEntity(model);

  list = () => this.get();
}

export default new AirportRepository();
