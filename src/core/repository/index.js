import BaseRepository from './base';
import FlightRepository from './flight';
import AirportRepository from './airport';

export { BaseRepository, FlightRepository, AirportRepository };
