import BaseRepository from './base';
import { FlightService } from '../service';
import { FlightEntity, SearchEntity } from '../entity';

class FlightRepository extends BaseRepository {
  getServiceProvider() {
    return FlightService;
  }

  modelToEntity = model => new FlightEntity(model);

  create = (params) => {
    const entity = new SearchEntity(params);

    return this.post(entity.getServiceObject());
  }
}

export default new FlightRepository();
