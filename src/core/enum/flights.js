export default {
  BSB: "Aeroporto Internacional Juscelino Kubitschek",
  VIX: "Aeroporto Eurico de Aguiar Salles",
  MCZ: "Aeroporto Internacional Zumbi dos Palmares",
  BEL: "Aeroporto Internacional de Belém de Cans",
  FLN: "Aeroporto Internacional Hercílio Luz",
  CGH: "Aeroporto de Congonhas Paulo",
  AJU: "Aeroporto Internacional de Aracaju",
  VCP: "Aeroporto Internacional de Viracopos",
  PMW: "Aeroporto de Palmas",
  PLU: "Aeroporto da Pampulha"
};
