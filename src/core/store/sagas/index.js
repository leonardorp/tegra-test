import { takeEvery, all } from 'redux-saga/effects';

import { Types as AirportTypes } from '../ducks/airports';
import { Types as FlightTypes } from '../ducks/flights';

import { getAirports } from './airports';
import { getFlights } from './flights';

export default function* root() {
  yield all([
    takeEvery(AirportTypes.FETCH_AIRPORTS, getAirports),
    takeEvery(FlightTypes.FETCH_FLIGHTS, getFlights),
  ]);
}
