import { put, call } from 'redux-saga/effects';

import { Types } from '../ducks/airports';

import { AirportRepository } from '../../repository';

export function* getAirports() {
  try {
    const response = yield call(AirportRepository.list);

    yield put({ type: Types.SUCCESS_FETCH_AIRPORTS, payload: response });
  } catch (err) {
    yield put({ type: Types.FAILURE_FETCH_AIRPORTS });
  }
}
