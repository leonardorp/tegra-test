import { put, call } from 'redux-saga/effects';

import { Types } from '../ducks/flights';

import { FlightRepository } from '../../repository';

export function* getFlights(action) {
  try {
    const response = yield call(FlightRepository.create, action.informations);

    yield put({ type: Types.SUCCESS_FETCH_FLIGHTS, payload: response });
  } catch (err) {
    yield put({ type: Types.FAILURE_FETCH_FLIGHTS });
  }
}
