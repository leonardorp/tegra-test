import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import airports from './airports';
import flights from './flights';

export default history => combineReducers({ router: connectRouter(history), airports, flights });
