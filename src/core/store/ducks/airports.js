import { createActions, createReducer } from 'reduxsauce';

// Action types & creators
export const { Types, Creators } = createActions({
  fetchAirports: [],
  failureFetchAirports: [],
  successFetchAirports: ['airports'],
});

// Handlers
const INITIAL_STATE = { loading: false, error: false, data: null };

const get = (state = INITIAL_STATE) => ({ ...state, loading: true });

const add = (state = INITIAL_STATE, action) => ({
  loading: false,
  error: false,
  data: [...action.payload.data],
});

const failure = () => ({ loading: false, error: true, data: [] });

// Reducer
export default createReducer(INITIAL_STATE, {
  [Types.FETCH_AIRPORTS]: get,
  [Types.SUCCESS_FETCH_AIRPORTS]: add,
  [Types.FAILURE_FETCH_AIRPORTS]: failure,
});
