import BaseService from './base';

class AirportService extends BaseService {
  async list() {
    const airports = await this.get('/flight/companies');

    return airports;
  }
}

export default new AirportService();
