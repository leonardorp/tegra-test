import BaseService from './base';

class FlightService extends BaseService {
  async create(params) {
    const flights = await this.post('/flight', params);

    return flights;
  }
}

export default new FlightService();
