import BaseEntity from './base';
import AirportEntity from './airport';
import FlightEntity from './flight';
import SearchEntity from './search';

export {
  BaseEntity, AirportEntity, FlightEntity, SearchEntity,
};
