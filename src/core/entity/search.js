import Moment from 'moment';
import BaseEntity from './base';

class AirportEntity extends BaseEntity {
  getServiceObject() {
    const parseDate = this.date.toISOString();
    const date = new Moment(parseDate).format("YYYY-MM-DD").toString();

    return {
      from: this.from,
      to: this.to,
      date,
    };
  }
}

export default AirportEntity;
