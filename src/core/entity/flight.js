import BaseEntity from './base';

import Moment from 'moment';
import Big from 'big.js';

class FlightEntity extends BaseEntity {
  defaultParams() {
    return {
      origem: '',
      destino: '',
      date: '',
      voos: [],
    };
  }

  defineFlightName() {
    if (this.voos.length > 1) {
      const flights = this.voos.map(actualVoo => actualVoo.voo);

      return flights.join(' - ');
    }

    return this.voos[0].voo;
  }

  parseDate() {
    return Moment(this.date).format("DD/MM/YYYY");
  }

  totalFlightValue() {
    if (this.voos.length > 1) {
      const total = this.voos.reduce((total, currentFlight) => Big(total).plus(currentFlight.valor), Big(0));

      return total.toString().replace('.', ',');
    }

    return this.voos[0].valor.toString().replace('.', ',');
  }

  totalFlightTime() {
    if (this.voos.length > 1) {
      const startDate = Moment(`${this.voos[0].data_saida} ${this.voos[0].saida}`, 'YYYY-MM-DD HH:mm:ss');
      const endDate = Moment(`${this.voos[0].data_saida} ${this.voos[this.voos.length - 1].chegada}`, 'YYYY-MM-DD HH:mm:ss');
      const duration = Moment.duration(endDate.diff(startDate));
      const hours = duration.asHours();
      
      return Math.floor(hours);
    }

    const startDate = Moment(`${this.voos[0].data_saida} ${this.voos[0].saida}`, 'YYYY-MM-DD HH:mm:ss');
    const endDate = Moment(`${this.voos[0].data_saida} ${this.voos[0].chegada}`, 'YYYY-MM-DD HH:mm:ss');
    const duration = Moment.duration(endDate.diff(startDate));
    const hours = duration.asHours();       
    
    return Math.floor(hours);
  }
}

export default FlightEntity;
