import BaseEntity from './base';

class AirportEntity extends BaseEntity {
  defaultParams() {
    return {
      nome: '',
      aeroporto: '',
      cidade: '',
    };
  }
}

export default AirportEntity;
