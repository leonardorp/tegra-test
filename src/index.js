import React from 'react';
import ReactDOM from 'react-dom';
import { install } from '@material-ui/styles';
import Moment from 'moment';
import App from './app';

import 'moment/locale/pt-br';

Moment.locale('pt-br');

install();

ReactDOM.render(<App />, document.getElementById('root'));
