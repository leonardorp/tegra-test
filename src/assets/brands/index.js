import ForbiddenAirplane from './forbidden-airplane.png';
import AirplaneTakingOff from './airplane-taking-off.jpg';

export default {
  ForbiddenAirplane,
  AirplaneTakingOff,
};
