import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';

import { theme } from '~/components/config';
import styles from './styles';

const SelectField = ({ label, onChange, value, options, classes }) => (
  <TextField
    className={classes.field}
    select
    label={label}
    value={value}
    onChange={event => onChange(event.target.value)}
    SelectProps={{ native: true }}
    margin="normal"
    variant="outlined"
  >
    {options.map(option => (
      <option key={option.value} value={option.value}>
        {option.label}
      </option>
    ))}
  </TextField>
);

SelectField.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  options: PropTypes.array,
};

export default theme(withStyles(styles)(SelectField));
