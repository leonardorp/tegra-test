import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import brands from '~/assets/brands';

import { FlightsEnum } from '~/core/enum';

import { theme } from '~/components/config';
import styles from './styles';

const CardFlights = ({ title, date, flight, totalValue, flightTime, flights, classes }) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <Card className={expanded ? classes.cardExpanded : classes.card}>
      <CardHeader
        title={title}
        subheader={date}
      />
      <CardMedia
        className={classes.media}
        image={brands.AirplaneTakingOff}
        title={flight}
      />
      <CardContent>
        <Typography paragraph>
          {`Tempo de Vôo: Cerca de ${flightTime}h`}
        </Typography>

        <Typography paragraph>
          {`Valor Total: R$ ${totalValue}`}
        </Typography>
      </CardContent>
      <CardActions className={classes.actions} disableActionSpacing>
        <IconButton
          className={classnames(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={() => setExpanded(!expanded)}
          aria-expanded={expanded}
          aria-label="Mostrar mais"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>{flights.length > 1 ? 'Escalas:' : 'Escala:'}</Typography>
          {
            flights.map((flight, index) => (
              <div key={index} className={classes.cardSpacing}>
                <Typography>{`Vôo: ${flight.voo}`}</Typography>
                <Typography>{`Origem: ${FlightsEnum[flight.origem]}`}</Typography>
                <Typography>{`Destino: ${FlightsEnum[flight.destino]}`}</Typography>
                <Typography>{`Saida: ${flight.saida}h -- Chegada: ${flight.chegada}h`}</Typography>
                <Typography>{`Valor: R$ ${flight.valor.toString().replace('.', ',')}`}</Typography>
              </div>
            ))
          }
        </CardContent>
      </Collapse>
    </Card>
  );
};

CardFlights.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string,
  date: PropTypes.string,
  flight: PropTypes.string,
  totalValue: PropTypes.string,
  flightTime: PropTypes.number,
  flights: PropTypes.array,
};

export default theme(withStyles(styles)(CardFlights));
