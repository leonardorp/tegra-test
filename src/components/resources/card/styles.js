const styles = theme => ({
  card: {
    width: 400,
    margin: 20,
    height: 500
  },
  cardExpanded: {
    height: 'auto',
    width: 400,
    margin: 20,
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  cardSpacing: {
    marginTop: 15
  },
});

export default styles;
