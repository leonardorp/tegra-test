import React from 'react';
import PropTypes from 'prop-types';

import { InlineDatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';

import { withStyles } from '@material-ui/styles';

import { theme } from '~/components/config';
import styles from './styles';

const DatePicker = ({ label, value, onChange, classes }) => (
  <MuiPickersUtilsProvider utils={MomentUtils}>
    <InlineDatePicker
      keyboard
      clearable
      className={classes.picker}
      variant="outlined"
      label={label}
      value={value}
      onChange={onChange}
      format="DD/MM/YYYY"
      mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
    />
  </MuiPickersUtilsProvider>
);

DatePicker.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func,
};

export default theme(withStyles(styles)(DatePicker));
