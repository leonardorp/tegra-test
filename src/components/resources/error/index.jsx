import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/styles';

import { theme } from '~/components/config';
import styles from './styles';

import brands from '~/assets/brands';

const Error = ({ classes }) => (
  <Fragment>
    <img src={brands.ForbiddenAirplane} alt="Não há vôos para essa data" className={classes.image} />
    <Typography variant="h6" color="inherit">
      Ooops...
    </Typography>
    <Typography variant="h6" color="inherit">
      Não há vôos para essa data :/
    </Typography>
  </Fragment>
);

Error.propTypes = {
  classes: PropTypes.object,
};

export default theme(withStyles(styles)(Error));
