import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import Icon from '@material-ui/core/Icon';

import { theme } from '~/components/config';
import styles from './styles';

const Buttons = ({ label, icon, onClick, classes }) => (
  <Button variant="contained" color="primary" className={classes.button} onClick={onClick}>
    {label}
    <Icon className={classes.rightIcon}>{icon}</Icon>
  </Button>
);

Button.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  icon: PropTypes.string,
  onClick: PropTypes.func,
};

export default theme(withStyles(styles)(Buttons));
