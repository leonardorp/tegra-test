export default {
  button: {
    width: '100%',
    marginTop: 23,
  },
  rightIcon: {
    marginLeft: 10,
  },
};
