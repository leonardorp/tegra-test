import React from 'react';
import PropTypes from 'prop-types';

import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/styles';

import { theme } from '~/components/config';
import styles from './styles';

const backgroundStyle = loading => loading ? { display: 'block', opacity: '1' } : { display: 'none', opacity: '0' };

const customSizeStyle = customSize => customSize ? customSize : 50;

const customThicknessStyle = customThickness => customThickness ? customThickness : 5;

const Loadable = ({ children, classes, loading, customSize, customThickness }) => (
  <div className={classes.wrapper}>
    {children}

    <div className={classes.background} style={backgroundStyle(loading)}>
      <div className={classes.loader}>
        <CircularProgress size={customSizeStyle(customSize)} thickness={customThicknessStyle(customThickness)} />
      </div>
    </div>
  </div>
);

Loadable.propTypes = {
  loading: PropTypes.bool,
  customSize: PropTypes.number,
  customThickness: PropTypes.number,
  children: PropTypes.node,
  classes: PropTypes.object,
};

export default theme(withStyles(styles)(Loadable));
