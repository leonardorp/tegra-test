import React, { Fragment } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import { theme } from '~/components/config';

const SimpleAppBar = () => (
  <Fragment>
    <AppBar position="static" color="primary">
      <Toolbar>
        <Typography variant="h6" color="inherit">
          Tegra Flights
        </Typography>
      </Toolbar>
    </AppBar>
  </Fragment>
);

export default theme(SimpleAppBar);
