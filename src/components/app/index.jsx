import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Col, Row } from 'react-flexbox-grid';

import { Creators as CreatorsAirports } from '~/core/store/ducks/airports';
import { Creators as CreatorsFlights } from '~/core/store/ducks/flights';

import { Header, SelectField, Button, DatePicker, Loadable, If, Error, Card } from '~/components/resources';

import { withStyles } from '@material-ui/styles';
import styles from './styles';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      from: 'none',
      to: 'none',
      date: new Date(),
      options: [],
    };
  }

  async componentDidMount() {
    await this.props.fetchAirports();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.airports.data !== this.props.airports.data) {
      const allAirports = this.tranformListOfAirportsToSelect(nextProps.airports.data);

      this.setState({ options: [{ label: 'Selecione', value: 'none' }, ...allAirports]});
    }
  }

  handleFieldChange(field, value) {
    this.setState({ [field]: value });
  }

  async handlePressButton() {
    const { fetchFlights } = this.props;
    const { from, to, date } = this.state;

    await fetchFlights({ from, to, date });
  }

  tranformListOfAirportsToSelect(airports) {
    return airports.map(airport => ({ label: airport.nome, value: airport.aeroporto }));
  }

  mountCardList(flights) {
    return flights.map((flight, index) => <Card key={index} title={`${flight.origem} - ${flight.destino}`} date={flight.parseDate()} flight={flight.defineFlightName()} totalValue={flight.totalFlightValue()} flightTime={flight.totalFlightTime()} flights={flight.voos} />);
  }

  render() {
    const { flights, airports, classes } = this.props;
    const { from, to, date, options } = this.state;

    return (
      <Fragment>
        <Header />
        <Loadable loading={(flights.loading || airports.loading) && true}>
          <Row className={classes.containerSearch}>
            <Col xs={12}>
              <Row center="xs">
                <Col xs={12} md={3}>
                  <SelectField value={from} onChange={value => this.handleFieldChange('from', value)} label="Aeroporto de Origem" options={options} />
                </Col>
                <Col xs={12} md={3}>
                  <SelectField value={to} onChange={value => this.handleFieldChange('to', value)} label="Aeroporto de Destino" options={options} />
                </Col>
                <Col xs={12} md={3}>
                  <DatePicker value={date} onChange={value => this.handleFieldChange('date', value)} label="Data de Saida" />
                </Col>
                <Col xs={12} md={1}>
                  <Button label="Buscar" icon="search" onClick={() => this.handlePressButton()} />
                </Col>
              </Row>
            </Col>
          </Row>

          <If test={(flights.data !== null && flights.data.length === 0) || flights.error}>
            <Row className={classes.containerError}>
              <Col xs={12}>
                <Row center="xs">
                  <Col xs={12}>
                    <Error />
                  </Col>
                </Row>
              </Col>
            </Row>
          </If>

          <If test={flights.data && flights.data.length > 0 && !flights.error}>
            <div className={classes.containerCards}>
              { (flights.data && flights.data.length > 0 && !flights.error) && this.mountCardList(flights.data) }
            </div>
          </If>

        </Loadable>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  fetchAirports: bindActionCreators(CreatorsAirports.fetchAirports, dispatch),
  fetchFlights: bindActionCreators(CreatorsFlights.fetchFlights, dispatch),
});

App.propTypes = {
  classes: PropTypes.object,
  flights: PropTypes.object,
  airports: PropTypes.object,
  fetchAirports: PropTypes.func,
  fetchFlights: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));
