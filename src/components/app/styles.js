export default {
  containerSearch: {
    margin: 20,
  },
  containerError: {
    marginTop: 50,
  },
  containerCards: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
};
