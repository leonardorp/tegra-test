import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#F4796B',
      main: '#FF2958',
      dark: '#F40000',
    },
    secondary: {
      light: grey[400],
      main: grey[500],
      dark: grey[600],
    },
  },
  typography: {
    useNextVariants: true,
  },
});

const withRoot = (Component) => {
  const WithRoot = props => (
    <MuiThemeProvider theme={theme}>
      <Component {...props} />
    </MuiThemeProvider>
  );

  return WithRoot;
};

export default withRoot;
