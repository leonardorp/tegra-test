export default [
  {"origem":"BSB","destino":"VCP","date":"2019-02-10","voos":[{"voo":"P243B976","origem":"BSB","destino":"VCP","data_saida":"2019-02-10","saida":"06:40","chegada":"19:40","valor":948.46}]},
  {"origem":"BSB","destino":"VCP","date":"2019-02-10","voos":[{"voo":"U264B986","origem":"BSB","destino":"AJU","data_saida":"2019-02-10","saida":"08:00","chegada":"15:40","valor":745.58},{"voo":"P250U946","origem":"AJU","destino":"VCP","data_saida":"2019-02-10","saida":"11:00","chegada":"14:40","valor":422.58}]},
  {"origem":"BSB","destino":"VCP","date":"2019-02-10","voos":[{"voo":"N207B980","origem":"BSB","destino":"FLN","data_saida":"2019-02-10","saida":"09:00","chegada":"19:20","valor":1347.21},{"voo":"P212N917","origem":"FLN","destino":"VCP","data_saida":"2019-02-10","saida":"10:20","chegada":"17:40","valor":572.92}]}
];
