import reducer, { Types } from '../../../../core/store/ducks/airports';
import dataAirports from '../../../__helpers__/data-airports';
import '../../../../components/config/specs';

describe('[Ducks] Airport', () => {
  const payload = {
    data: dataAirports,
  };

  it('should handle initial state', () => {
    expect(reducer(undefined, {})).toEqual({ loading: false, error: false, data: null });
  });

  it('should handle SUCCESS_FETCH_AIRPORTS action', () => {
    expect(reducer({}, { type: Types.SUCCESS_FETCH_AIRPORTS, payload })).toEqual({ loading: false, error: false, data: dataAirports });
  });

  it('should handle FEILURE_FETCH_AIRPORTS action', () => {
    expect(reducer({}, { type: Types.FEILURE_FETCH_AIRPORTS })).toEqual({});
  });
});
