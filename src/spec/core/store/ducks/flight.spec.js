import '../../../../components/config/specs';

import { call } from 'redux-saga/effects';
import { getFlights } from '../../../../core/store/sagas/flights';
import { FlightRepository } from '../../../../core/repository';
import dataSearch from '../../../__helpers__/data-search';

describe('[Sagas] Flight', () => {
  it('should take fetch flight success', (done) => {
    const saga = getFlights({ informations: dataSearch });
    const output = saga.next().value;
    const expected = call(FlightRepository.create, dataSearch);

    done();
    expect(output).toEqual(expected);
  });
});
