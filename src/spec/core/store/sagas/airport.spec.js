import '../../../../components/config/specs';

import { call } from 'redux-saga/effects';
import { getAirports } from '../../../../core/store/sagas/airports';
import { AirportRepository } from '../../../../core/repository';

describe('[Sagas] Airport', () => {
  const saga = getAirports();

  let output = null;

  it('should take fetch aiports success', (done) => {
    output = saga.next().value;
    const expected = call(AirportRepository.list);
    done();
    expect(output).toEqual(expected);
  });
});
